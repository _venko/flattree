use crate::traits::Order;

pub struct Preorder;
pub struct Inorder;
pub struct Postorder;

impl Order for Preorder {}
impl Order for Inorder {}
impl Order for Postorder {}
