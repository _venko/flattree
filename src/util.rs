use itertools::Itertools;

pub(crate) trait ShiftSlice {
    fn shift_slice(&mut self, src: usize, len: usize, dest: usize);
}

impl<T> ShiftSlice for Vec<T> {
    fn shift_slice(&mut self, src: usize, len: usize, dest: usize) {
        let src_range = src..src + len;
        let dest_range = dest..dest;
        let elems = self.drain(src_range).collect_vec();
        self.splice(dest_range, elems);
    }
}
