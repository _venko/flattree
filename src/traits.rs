/// Marker trait for types that can be used to indicate a tree iteration order
/// (eg: pre-order, post-order, level-order).
pub trait Order {}

pub trait Handles<T, O>
where
    O: Order,
{
    /// Returns an iterator of every handle into a tree.
    ///
    /// A handle is an index that can be used to refer to a specific node
    /// within the tree.
    fn handles(&self) -> impl Iterator<Item = T>;
}

pub trait Elem<'a, E, H, O>: Handles<H, O>
where
    E: 'a,
    O: Order,
{
    /// Returns a reference to the value stored in the node referred to by the
    /// given `handle`.
    fn elem(&self, handle: H) -> &E;
}

pub trait Depth<H, O>: Handles<H, O>
where
    O: Order,
{
    /// Gets the depth of the tree node referred to by the given `handle`.
    fn depth(&self, handle: H) -> usize;
}

pub trait Parent<H, O>: Handles<H, O>
where
    O: Order,
{
    /// Gets the parent of the node referred to by the given `handle`, if one exists.
    fn parent(&self, handle: H) -> Option<H>;
}

pub trait Ancestor<H, O>: Handles<H, O>
where
    O: Order,
{
    /// Returns an iterator of handles to the full sequence of nodes from a
    /// node at `handle` to a root, starting from its parent.
    fn ancestors(&self, handle: H) -> impl Iterator<Item = H>;
}

pub trait Siblings<H, O>: Handles<H, O>
where
    O: Order,
{
    /// Returns an iterator of handles to all the nodes sharing the same parent
    /// as the one at `handle` that are to the node at `handle`'s left.
    fn left_siblings(&self, handle: H) -> impl Iterator<Item = H>;

    /// Returns an iterator of handles to all the nodes sharing the same parent
    /// as the one at `handle` that are to the node at `handle`'s right.
    fn right_siblings(&self, handle: H) -> impl Iterator<Item = H>;
}

pub trait Children<H, O>: Handles<H, O>
where
    O: Order,
{
    /// Returns an iterator of handles to the nodes that descend one level below
    /// the node at `handle`.
    fn children(&self, handle: H) -> impl Iterator<Item = H>;
}

pub trait Descendents<H, O>: Handles<H, O>
where
    O: Order,
{
    /// Returns an iterator of handles to the nodes that descend all levels
    /// below the node at `handle`. This is effectively the subtree starting at
    /// `handle`, except the node at `handle` is not included in the iterator.
    ///
    /// The order of handles produced by the returned iterator is determined by
    /// `O`.
    fn descendents(&self, handle: H) -> impl Iterator<Item = H>;
}
