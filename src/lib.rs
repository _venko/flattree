extern crate itertools;

mod bst;
mod vectree;

pub mod order;
pub mod traits;
pub mod util;

use util::ShiftSlice;

pub use bst::{Bst, BstErr};
pub use vectree::{Idx, VecTree};
