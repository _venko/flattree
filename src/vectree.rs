use std::marker::PhantomData;

pub use crate::{
    order::Preorder,
    traits::{Ancestor, Children, Depth, Descendents, Elem, Handles, Order, Parent, Siblings},
};

pub type Idx = usize;

// TODO: Remove `pub` on contents. Just make the API good instead :)
pub struct VecTree<T, O> {
    pub items: Vec<T>,
    pub depths: Vec<usize>,
    order: PhantomData<O>,
}

impl<T, O> VecTree<T, O>
where
    O: Order,
{
    pub fn new() -> VecTree<T, O> {
        VecTree::<T, O> {
            items: Vec::default(),
            depths: Vec::default(),
            order: PhantomData,
        }
    }

    pub fn with_capacity(capacity: usize) -> VecTree<T, O> {
        VecTree::<T, O> {
            items: Vec::with_capacity(capacity),
            depths: Vec::with_capacity(capacity),
            order: PhantomData,
        }
    }

    pub fn push(&mut self, item: T, depth: usize) {
        self.items.push(item);
        self.depths.push(depth);
    }

    pub fn insert_batch(
        &mut self,
        handle: Idx,
        items: impl IntoIterator<Item = T>,
        depths: impl IntoIterator<Item = usize>,
    ) {
        self.items.splice(handle..handle, items);
        self.depths.splice(handle..handle, depths);
    }

    pub fn insert(&mut self, handle: Idx, item: T, depth: usize) {
        self.items.insert(handle, item);
        self.depths.insert(handle, depth);
    }

    pub fn len(&self) -> usize {
        self.items.len()
    }
}

impl<T, O> Default for VecTree<T, O>
where
    O: Order,
{
    fn default() -> Self {
        Self::new()
    }
}

impl<T, O> Handles<Idx, O> for VecTree<T, O>
where
    O: Order,
{
    fn handles(&self) -> impl Iterator<Item = Idx> {
        0..self.items.len()
    }
}

impl<'a, T, O> Elem<'a, T, Idx, O> for VecTree<T, O>
where
    T: 'a,
    O: Order,
{
    fn elem(&self, handle: Idx) -> &T {
        &self.items[handle]
    }
}

impl<T, O> Depth<Idx, O> for VecTree<T, O>
where
    O: Order,
{
    fn depth(&self, handle: Idx) -> usize {
        self.depths[handle]
    }
}

impl<T> Parent<Idx, Preorder> for VecTree<T, Preorder> {
    fn parent(&self, handle: Idx) -> Option<Idx> {
        match handle {
            0 => None,
            _ => {
                let parent_depth = self.depth(handle) - 1;
                (0..handle).rev().find(|&h| self.depth(h) == parent_depth)
            }
        }
    }
}

struct AncestorIterator<'a, T> {
    cursor: Option<Idx>,
    tree: &'a VecTree<T, Preorder>,
}

impl<'a, T> Iterator for AncestorIterator<'a, T> {
    type Item = Idx;

    fn next(&mut self) -> Option<Self::Item> {
        let old_cursor = self.cursor?;
        self.cursor = self.tree.parent(old_cursor);
        self.cursor
    }
}

impl<T> Ancestor<Idx, Preorder> for VecTree<T, Preorder> {
    fn ancestors(&self, handle: Idx) -> impl Iterator<Item = Idx> {
        AncestorIterator {
            cursor: (handle != 0).then_some(handle),
            tree: self,
        }
    }
}

struct Left;
struct Right;

trait Direction {}

impl Direction for Left {}
impl Direction for Right {}

trait SiblingCursor {
    fn get_next_cursor(&self, cursor: Idx) -> Option<Idx>;
}

struct SiblingIterator<'a, T, D>
where
    D: Direction,
{
    cursor: Option<Idx>,
    tree: &'a VecTree<T, Preorder>,
    direction: PhantomData<D>,
}

impl<'a, T, D> Iterator for SiblingIterator<'a, T, D>
where
    D: Direction,
    Self: SiblingCursor,
{
    type Item = Idx;

    fn next(&mut self) -> Option<Self::Item> {
        match self.cursor? {
            0 => None,
            old_cursor => {
                self.cursor = self.get_next_cursor(old_cursor);

                while let Some(cursor) = self.cursor {
                    let same_parents = self.tree.parent(cursor) == self.tree.parent(old_cursor);
                    let same_depth = self.tree.depth(cursor) == self.tree.depth(old_cursor);

                    if same_depth && same_parents {
                        self.cursor = Some(cursor);
                        break;
                    }

                    self.cursor = self.get_next_cursor(cursor);
                }

                self.cursor
            }
        }
    }
}

type LeftSiblingIterator<'a, T> = SiblingIterator<'a, T, Left>;

impl<'a, T> SiblingCursor for LeftSiblingIterator<'a, T> {
    fn get_next_cursor(&self, cursor: Idx) -> Option<Idx> {
        (cursor > 0).then_some(cursor - 1)
    }
}

type RightSiblingIterator<'a, T> = SiblingIterator<'a, T, Right>;

impl<'a, T> SiblingCursor for RightSiblingIterator<'a, T> {
    fn get_next_cursor(&self, cursor: Idx) -> Option<Idx> {
        (cursor < self.tree.items.len() - 1).then_some(cursor + 1)
    }
}

impl<T> Siblings<Idx, Preorder> for VecTree<T, Preorder> {
    fn left_siblings(&self, handle: Idx) -> impl Iterator<Item = Idx> {
        LeftSiblingIterator {
            cursor: (handle != 0).then_some(handle),
            tree: self,
            direction: PhantomData,
        }
    }

    fn right_siblings(&self, handle: Idx) -> impl Iterator<Item = Idx> {
        RightSiblingIterator {
            cursor: (handle != 0).then_some(handle),
            tree: self,
            direction: PhantomData,
        }
    }
}

impl<T> Children<Idx, Preorder> for VecTree<T, Preorder> {
    fn children(&self, handle: Idx) -> impl Iterator<Item = Idx> {
        let min_depth = self.depths[handle];
        self.depths
            .iter()
            .enumerate()
            .skip(handle + 1)
            .take_while(move |(_, &depth)| depth > min_depth)
            .filter_map(move |(i, &depth)| (depth == min_depth + 1).then_some(i))
    }
}

impl<T> Descendents<Idx, Preorder> for VecTree<T, Preorder> {
    fn descendents(&self, handle: Idx) -> impl Iterator<Item = Idx> {
        let min_depth = self.depths[handle];
        self.depths
            .iter()
            .enumerate()
            .skip(handle + 1)
            .take_while(move |(_, &depth)| depth > min_depth)
            .map(|(handle, _)| handle)
    }
}
