use flattree::Bst;
use itertools::Itertools;
use rand::{thread_rng, Rng};

fn main() {
    let mut rng = thread_rng();
    let size = 250_000;
    println!("init test data");
    let nums: Vec<usize> = (0..size).map(|_| rng.gen()).collect_vec();
    println!("done");
    loop {
        let mut tree = Bst::with_capacity(size);
        let now = std::time::Instant::now();
        _ = tree.insert_batch(nums.iter());
        while let Some(min) = tree.minimum() {
            _ = tree.delete(min);
        }
        let elapsed = std::time::Instant::now() - now;
        println!(
            "{} ({} ns)",
            pretty_duration::pretty_duration(&elapsed, None),
            elapsed.as_nanos()
        );
    }
}
