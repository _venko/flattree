use std::{
    borrow::Borrow,
    cmp::Ordering::{Equal, Greater, Less},
    fmt::{Debug, Display},
};

use itertools::Itertools;

use crate::{
    order::Preorder,
    traits::{Ancestor, Children, Depth, Descendents, Elem, Handles, Parent, Siblings},
    Idx, ShiftSlice, VecTree,
};

#[derive(Debug, PartialEq, Eq)]
pub enum BstErr {
    InvalidHandle,
    ItemAlreadyPresent,
    NoSuchItem,
}

enum NodePattern {
    Leaf,
    OneChild(Idx),
    TwoChildren(Idx, Idx),
}

pub struct Bst<T>(VecTree<T, Preorder>);

mod trait_impls {
    use super::*;

    impl<T> Handles<Idx, Preorder> for Bst<T> {
        fn handles(&self) -> impl Iterator<Item = Idx> {
            self.0.handles()
        }
    }

    impl<'a, T> Elem<'a, T, Idx, Preorder> for Bst<T>
    where
        T: 'a,
    {
        fn elem(&self, handle: Idx) -> &T {
            self.0.elem(handle)
        }
    }

    impl<T> Depth<Idx, Preorder> for Bst<T> {
        fn depth(&self, handle: Idx) -> usize {
            self.0.depth(handle)
        }
    }

    impl<T> Parent<Idx, Preorder> for Bst<T> {
        fn parent(&self, handle: Idx) -> Option<Idx> {
            self.0.parent(handle)
        }
    }

    impl<T> Ancestor<Idx, Preorder> for Bst<T> {
        fn ancestors(&self, handle: Idx) -> impl Iterator<Item = Idx> {
            self.0.ancestors(handle)
        }
    }

    impl<T> Siblings<Idx, Preorder> for Bst<T> {
        fn left_siblings(&self, handle: Idx) -> impl Iterator<Item = Idx> {
            self.0.left_siblings(handle)
        }

        fn right_siblings(&self, handle: Idx) -> impl Iterator<Item = Idx> {
            self.0.right_siblings(handle)
        }
    }

    impl<T> Children<Idx, Preorder> for Bst<T> {
        fn children(&self, handle: Idx) -> impl Iterator<Item = Idx> {
            self.0.children(handle)
        }
    }

    impl<T> Descendents<Idx, Preorder> for Bst<T> {
        fn descendents(&self, handle: Idx) -> impl Iterator<Item = Idx> {
            self.0.descendents(handle)
        }
    }
}

impl<T> Display for Bst<T>
where
    T: Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for handle in self.handles() {
            write!(f, "{handle:>3}: ")?;
            for i in 0..self.depth(handle) {
                if i == self.depth(handle) - 1 {
                    write!(f, "|-")?;
                } else {
                    write!(f, "| ")?;
                }
            }
            writeln!(f, "{{{}}}", self.elem(handle))?;
        }
        Ok(())
    }
}

impl<T> Bst<T>
where
    T: Ord
{
    pub fn new() -> Bst<T> {
        Bst(VecTree::new())
    }

    pub fn with_capacity(capacity: usize) -> Bst<T> {
        Bst::<T>(VecTree::with_capacity(capacity))
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    fn root(&self) -> Option<usize> {
        (self.len() > 0).then_some(0)
    }

    pub fn minimum(&self) -> Option<&T> {
        let root = self.root()?;
        let min_idx = self.minimum_from(root);
        Some(self.elem(min_idx))
    }

    pub fn maximum(&self) -> Option<&T> {
        let root = self.root()?;
        let max_idx = self.maximum_from(root);
        Some(self.elem(max_idx))
    }

    pub fn contains<B: Borrow<T>>(&self, key: B) -> bool {
        self.search(key).is_some()
    }

    /// Searches this tree for the value `key`, returning the index if the tree
    /// contains the key, otherwise None.
    fn search<B: Borrow<T>>(&self, key: B) -> Option<Idx> {
        let key = key.borrow();
        let mut current_node = self.root();
        while let Some(cursor) = current_node {
            match key.cmp(self.elem(cursor)) {
                Equal => break,
                Less => current_node = self.left_child(cursor),
                Greater => current_node = self.right_child(cursor),
            }
        }
        current_node
    }

    pub fn insert_batch(&mut self, items: impl IntoIterator<Item = T>) -> Result<(), BstErr> {
        for item in items {
            let mut cursor = None;
            let mut peek = self.root();
            while let Some(idx) = peek {
                cursor = peek;
                match item.cmp(self.elem(idx)) {
                    Equal => return Err(BstErr::ItemAlreadyPresent),
                    Less => peek = self.left_child(idx),
                    Greater => peek = self.right_child(idx),
                }
            }

            let Some(cursor) = cursor else {
                self.push(item, 0);
                continue;
            };

            if item < *self.elem(cursor) {
                self.set_left(cursor, item);
            } else {
                self.set_right(cursor, item);
            }
        }
        Ok(())
    }

    pub fn insert(&mut self, item: T) -> Result<(), BstErr> {
        self.insert_batch(std::iter::once(item))
    }

    pub fn delete_batch(&mut self, items: impl IntoIterator<Item = T>) -> Result<(), BstErr> {
        for item in items.into_iter() {
            let Some(handle) = self.search(item) else {
                return Err(BstErr::NoSuchItem);
            };

            match self.match_node(handle) {
                NodePattern::Leaf => {
                    self.0.items.remove(handle);
                    self.0.depths.remove(handle);
                }
                NodePattern::OneChild(child_handle) => {
                    self.lift_subtree(child_handle, 1);
                    self.0.items.remove(handle);
                    self.0.depths.remove(handle);
                }
                NodePattern::TwoChildren(_, _) => {
                    let successor = self
                        .successor(handle)
                        .expect("Node with children must have a successor.");

                    if matches!(self.match_node(successor), NodePattern::OneChild(_)) {
                        self.lift_subtree(successor, 1);
                    }

                    self.swap_nodes(handle, successor);
                    self.0.items.remove(successor);
                    self.0.depths.remove(successor);
                }
            }
        }

        Ok(())
    }

    /// Deletes the node at the given index, returning the value held in the
    /// node.
    pub fn delete(&mut self, item: T) -> Result<(), BstErr> {
        self.delete_batch(std::iter::once(item))
    }

    fn push_batch(
        &mut self,
        items: impl IntoIterator<Item = T>,
        depths: impl IntoIterator<Item = usize>,
    ) {
        for (item, depth) in items.into_iter().zip(depths.into_iter()) {
            self.0.push(item, depth);
        }
    }

    fn push(&mut self, item: T, depth: usize) {
        self.0.push(item, depth);
    }

    /// Finds the node in the tree whose value is the smallest value in the tree
    /// that is greater than the value at `handle`, or None if no such node exists.
    fn successor(&self, handle: Idx) -> Option<Idx> {
        if let Some(right) = self.right_child(handle) {
            return Some(self.minimum_from(right));
        }

        let mut cursor = handle;
        loop {
            match self.parent(cursor) {
                None => return None,
                Some(p) => match self.right_child(p) {
                    Some(right) if right == cursor => {
                        cursor = p;
                    }
                    _ => return Some(p),
                },
            }
        }
    }

    /// Finds the node in the tree whose value is the largest value in the tree
    /// that is less than the value at `handle`, or None if no such node exists.
    fn predecessor(&self, handle: Idx) -> Option<Idx> {
        if let Some(left) = self.left_child(handle) {
            return Some(self.maximum_from(left));
        }

        let mut cursor = handle;
        loop {
            match self.parent(cursor) {
                None => return None,
                Some(p) => match self.left_child(p) {
                    Some(left) if left == cursor => {
                        cursor = p;
                    }
                    _ => return Some(p),
                },
            }
        }
    }

    fn left_child(&self, handle: Idx) -> Option<Idx> {
        let Some(first) = self.children(handle).next() else {
            return None;
        };
        (*self.elem(first) < *self.elem(handle)).then_some(first)
    }

    fn right_child(&self, handle: Idx) -> Option<Idx> {
        let mut children = self.children(handle);
        let Some(first) = children.next() else {
            return None;
        };
        let Some(second) = children.next() else {
            return (*self.elem(first) > *self.elem(handle)).then_some(first);
        };
        Some(second)
    }

    fn lift_subtree(&mut self, handle: Idx, amount: usize) {
        let descendents = self.descendents(handle).collect_vec();
        self.0.depths[handle] -= amount;
        for descendent in descendents {
            self.0.depths[descendent] -= amount;
        }
    }

    fn lower_subtree(&mut self, handle: Idx, amount: usize) {
        let descendents = self.descendents(handle).collect_vec();
        self.0.depths[handle] += amount;
        for descendent in descendents {
            self.0.depths[descendent] += amount;
        }
    }

    fn minimum_from(&self, handle: Idx) -> Idx {
        let mut cursor = handle;
        while let Some(left) = self.left_child(cursor) {
            cursor = left
        }
        cursor
    }

    fn maximum_from(&self, handle: Idx) -> Idx {
        let mut cursor = handle;
        while let Some(right) = self.right_child(cursor) {
            cursor = right
        }
        cursor
    }

    fn set_left(&mut self, handle: Idx, item: T) -> Idx {
        let item_handle = handle + 1;
        self.0.insert(item_handle, item, self.depth(handle) + 1);
        item_handle
    }

    fn set_right(&mut self, handle: Idx, item: T) -> Idx {
        let Some(left_handle) = self.left_child(handle) else {
            let item_handle = handle + 1;
            self.0.insert(item_handle, item, self.depth(handle) + 1);
            return item_handle;
        };

        let left_len = self.descendents(left_handle).collect::<Vec<_>>().len();
        let item_handle = left_handle + left_len + 1;
        self.0.insert(item_handle, item, self.depth(handle) + 1);
        item_handle
    }

    fn match_node(&self, handle: Idx) -> NodePattern {
        let left = self.left_child(handle);
        let right = self.right_child(handle);
        match (left, right) {
            (None, None) => NodePattern::Leaf,
            (None, Some(h)) | (Some(h), None) => NodePattern::OneChild(h),
            (Some(lh), Some(rh)) => NodePattern::TwoChildren(lh, rh),
        }
    }

    fn swap_nodes(&mut self, handle0: Idx, handle1: Idx) {
        self.0.items.swap(handle0, handle1);
    }

    fn move_subtree(&mut self, subtree: Idx, dest: Idx) {
        let len = self.descendents(subtree).collect_vec().len() + 1;
        self.0.items.shift_slice(subtree, len, dest);
        self.0.depths.shift_slice(subtree, len, dest);
    }

    fn make_left_child(&mut self, subtree: Idx, dest: Idx) {
        let new_depth = self.depth(dest) + 1;
        let src_depth = self.depth(subtree);
        let depth_delta = new_depth as isize - src_depth as isize;
        if depth_delta > 0 {
            self.lower_subtree(subtree, depth_delta as usize);
        } else {
            self.lift_subtree(subtree, (-1 * depth_delta) as usize);
        }
        self.move_subtree(subtree, dest + 1);
    }

    fn make_right_child(&mut self, subtree: Idx, dest: Idx) {
        let new_depth = self.depth(dest) + 1;
        let src_depth = self.depth(subtree);
        let depth_delta = new_depth as isize - src_depth as isize;
        if depth_delta > 0 {
            self.lower_subtree(subtree, depth_delta as usize);
        } else {
            self.lift_subtree(subtree, (-1 * depth_delta) as usize);
        }
        let subtree_len = self.descendents(subtree).collect_vec().len();
        let dest_subtree_len = self.descendents(dest).collect_vec().len();
        let offset_dest = if dest > subtree {
            dest - subtree_len
        } else {
            dest
        };
        self.move_subtree(subtree, offset_dest + dest_subtree_len);
    }

    pub fn verify_bst_property(&self) {
        for handle in self.handles() {
            let elem = self.elem(handle);
            let count = self.children(handle).collect_vec().len();
            assert!(count < 3);
            if let Some(left_handle) = self.left_child(handle) {
                let left = self.elem(left_handle);
                assert!(elem > left);
            }
            if let Some(right_handle) = self.right_child(handle) {
                let right = self.elem(right_handle);
                assert!(elem < right);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn values() -> Vec<usize> {
        vec![8, 3, 1, 6, 4, 7, 10, 14, 13]
    }

    fn depths() -> Vec<usize> {
        vec![0, 1, 2, 2, 3, 3, 1, 2, 3]
    }

    fn test_tree() -> Bst<usize> {
        let mut tree: Bst<usize> = Bst::new();
        tree.push_batch(values(), depths());
        tree
    }

    #[test]
    fn maximum() {
        let tree = test_tree();
        assert!(tree.maximum().is_some_and(|&it| it == 14));
    }

    #[test]
    fn minimum() {
        let tree = test_tree();
        assert!(tree.minimum().is_some_and(|&it| it == 1));
    }

    #[test]
    fn contains() {
        let tree = test_tree();
        assert!(values().iter().all(|value| tree.contains(value)));
    }

    #[test]
    fn insert() {
        let mut tree = Bst::new();

        assert!(tree.insert(1).is_ok());
        assert!(tree.search(1).is_some());
        tree.verify_bst_property();

        assert!(tree.insert(0).is_ok());
        assert!(tree.search(0).is_some());
        tree.verify_bst_property();

        assert!(tree.insert(3).is_ok());
        assert!(tree.search(3).is_some());
        tree.verify_bst_property();

        assert!(tree.insert(2).is_ok());
        assert!(tree.search(2).is_some());
        tree.verify_bst_property();

        assert!(tree.insert(4).is_ok());
        assert!(tree.search(4).is_some());
        tree.verify_bst_property();
    }

    #[test]
    fn delete_node_with_no_children() {
        let mut tree = test_tree();
        assert!(tree.delete(3).is_ok());
        tree.verify_bst_property();
    }

    #[test]
    fn delete_node_with_left_child() {
        let mut tree = test_tree();
        assert!(tree.delete(14).is_ok());
        tree.verify_bst_property();
    }

    #[test]
    fn delete_node_with_right_child() {
        let mut tree = test_tree();
        assert!(tree.delete(10).is_ok());
        tree.verify_bst_property();
    }

    #[test]
    fn delete_non_root_node_with_both_children() {
        let mut tree = Bst::new();
        _ = tree.insert_batch(vec![1, 0, 3, 2, 9, 4, 5, 6]);
        assert!(tree.delete(3).is_ok());
        tree.verify_bst_property();
    }

    #[test]
    fn delete_root_node_with_both_children() {
        let mut tree = Bst::new();
        _ = tree.insert_batch(vec![1, 0, 3, 2, 9, 4, 5, 6]);
        assert!(tree.delete(0).is_ok());
        tree.verify_bst_property();
    }

    #[test]
    fn delete_root_node_with_both_children_alternate() {
        let mut tree = Bst::new();
        _ = tree.insert_batch(vec![4, 2, 1, 3, 5, 6]);
        assert!(tree.delete(4).is_ok());
        tree.verify_bst_property();
    }

    #[test]
    fn move_subtree_leftward() {
        let mut tree = Bst::new();
        tree.push_batch(vec![3, 1, 0, 2, 5, 4, 6], vec![0, 1, 2, 2, 1, 2, 2]);
        tree.move_subtree(4, 1);
        assert_eq!(tree.0.items, vec![3, 5, 4, 6, 1, 0, 2])
    }

    #[test]
    fn move_subtree_rightward() {
        let mut tree = Bst::new();
        tree.push_batch(vec![3, 1, 0, 2, 5, 4, 6], vec![0, 1, 2, 2, 1, 2, 2]);
        tree.move_subtree(1, 4);
        assert_eq!(tree.0.items, vec![3, 5, 4, 6, 1, 0, 2])
    }

    #[test]
    fn make_left_child() {
        let mut tree = Bst::new();
        tree.push_batch(vec![3, 1, 0, 2, 5, 4, 6], vec![0, 1, 2, 2, 1, 2, 2]);
        tree.make_left_child(/*src=*/ 4, /*dest=*/ 2);
        assert_eq!(tree.0.items, vec![3, 1, 0, 5, 4, 6, 2]);
        assert_eq!(tree.0.depths, vec![0, 1, 2, 3, 4, 4, 2]);
    }

    #[test]
    fn make_right_child() {
        let mut tree = Bst::new();
        tree.push_batch(vec![3, 1, 0, 2, 5, 4], vec![0, 1, 2, 2, 1, 2]);
        tree.make_right_child(/*src=*/ 1, /*dest=*/ 4);
        assert_eq!(tree.0.items, vec![3, 5, 4, 1, 0, 2]);
        assert_eq!(tree.0.depths, vec![0, 1, 2, 2, 3, 3]);
        println!("{tree}");
    }
}
